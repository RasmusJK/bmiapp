//
//  PersonViewController.swift
//  lab3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Rasmus Karling. All rights reserved.
//

import UIKit
import os.log

class PersonViewController: UIViewController, UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UINavigationControllerDelegate {

    
    
    //MARK: Properties
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var whPicker: UIPickerView!
    @IBOutlet weak var saveButton: UIBarButtonItem!
    @IBOutlet weak var bmiLabel: UILabel!
    var person: Person?


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        nameTextField.delegate = self
        whPicker.delegate = self
        whPicker.dataSource = self
        saveButtonstate()
    }
    
    //MARK: nameTextField delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.text?.isEmpty == true {
            let alert = UIAlertController(title: "Error", message: "Enter a name", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default))
            self.present(alert, animated: true, completion: nil)
        }
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        saveButton.isEnabled = false
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        saveButtonstate()
        textField.resignFirstResponder()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet.letters.inverted) != nil {
            return false
        }
        return true
    }
    
    
    //MARK: whPicker delegate
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return 70
        } else {
            return 160
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return "\(row + 130)"
        } else {
            return "\(row + 40)"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let name = nameTextField.text
        let height = (whPicker.selectedRow(inComponent: 0) + 130)
        let weight = (whPicker.selectedRow(inComponent: 1) + 40)
        
        let age = 20
        let professions: Set<String> = ["programmer"]
        person = Person(name: name ?? "", height: Double(height), weight: Double(weight), age: age, professions: professions)
    
        
        bmiLabel.text = "BMI: \(round((person?.calculateBMI() ?? 0.0) * 10) / 10)"
    }

    //MARK: Navigation
    @IBAction func cancel(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        super.prepare(for: segue, sender: sender)
        guard let button = sender as? UIBarButtonItem, button === saveButton else {
            os_log("The save button was not pressed, cancelling", log: OSLog.default, type: .debug)
            return
        }
        
        let name = nameTextField.text ?? ""
        let height = (whPicker.selectedRow(inComponent: 0) + 130)
        let weight = (whPicker.selectedRow(inComponent: 1) + 40)
        
        let age = 20
        let professions: Set<String> = ["programmer"]
        print(name, height, weight)
        
        person = Person(name: name, height: Double(height), weight: Double(weight), age: age, professions: professions)
    }
    
    
    //MARK: Private methods
    
    private func saveButtonstate() {
        let text = nameTextField.text ?? ""
        print(text)
        saveButton.isEnabled = !text.isEmpty
        
    }
}

