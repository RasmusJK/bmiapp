//
//  Person.swift
//  lab3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Rasmus Karling. All rights reserved.
//
import UIKit
import os.log

class Person {
    
    //MARK: Properties
     let name: String
    
    private var _height: Double
    private var _weight: Double
    private var _age : Int
    private var _professions :Set<String> = []
    
    var height : Double{
        get{
         return _height
        }
        set{
            if (newValue > 0 && newValue <= 300){
                _height = newValue
            } else{
                print("error")
            }
        }
    }
    
    var weight: Double{
        get{
            return _weight
        }
        set{
            if (newValue > 0) {
                _weight = newValue
            } else{
                print("error")
            }
        }
    }
    
    var age: Int {
        get{
            return _age
        }
        set{
            if (newValue > self.age){
                _age = newValue
                
            }else{
                print("error")
            }
        }
    }
    
    var professions : Set<String> {
        get{
            return _professions
        }
        set{
            if (newValue.count >= 1 && newValue.count <= 3){
                _professions = newValue
            } else{
                print("error")
            }
        }
    }
    
    
    
    //MARK: Initilization
    init?(name: String, height: Double, weight: Double, age: Int, professions: Set<String>) {
        //Name must not be empty
        if name.isEmpty || height <= 0 && height >= 300 ||  weight <= 0 || age <= 0 && age >= 130 || professions.count > 3  {
            return nil
        }
        
        //Initialize properties
        self.name = name
        self._weight = weight
        self._height = height
        self._age = age
        self._professions = professions
    }
    func addProfessions(profession : String) {
        if professions.count >= 3{
            print("cant add professions")
        } else {
            professions.insert(profession)
        }
    }
    
    //MARK: Functions
    func calculateBMI() -> Double {
        return weight / ((height / 100) * (height / 100))
    }
        
}
