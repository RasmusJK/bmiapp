//
//  PersonTableViewController.swift
//  lab3
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Rasmus Karling. All rights reserved.
//

import UIKit
import os.log

class PersonTableViewController: UITableViewController {
    
    //MARK: Properties
    var bmis = [Person]()

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return bmis.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cellIdentifier = "PersonTableViewCell"
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? PersonTableViewCell else {
                fatalError("Super fatal error")
        }
        
        let bmi = bmis[indexPath.row]
        
        cell.nameLabel.text = "Name: \(bmi.name)"
        cell.bmiLabel.text = "BMI: \(round(bmi.calculateBMI() * 10) / 10)"
        cell.heightLabel.text = "Height: \(bmi.height)cm"
        cell.weightLabel.text = "Weight: \(bmi.weight)kg"
        
        if bmi.calculateBMI() < 10.5 {
            cell.backgroundColor = UIColor.blue
        } else if bmi.calculateBMI() <= 25 {
            cell.backgroundColor = UIColor.green
        } else if bmi.calculateBMI() <= 30 {
            cell.backgroundColor = UIColor.orange
        } else {
            cell.backgroundColor = UIColor.red
        }
        
        return cell
    }
    
    //MARK: Actions
    
    @IBAction func unwindToHome(sender: UIStoryboardSegue) {
        if let source = sender.source as? PersonViewController, let person = source.person {
            let newIndexPath = IndexPath(row: bmis.count, section: 0)
            bmis.append(person)
            tableView.insertRows(at: [newIndexPath], with: .automatic)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            
            bmis.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    
    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
