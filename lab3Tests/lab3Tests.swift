//
//  lab3Tests.swift
//  lab3Tests
//
//  Created by iosdev on 30.3.2020.
//  Copyright © 2020 Rasmus Karling. All rights reserved.
//

import XCTest

@testable import lab3

class lab3Tests: XCTestCase {
//let person1 =  Person(name: "kaapo", height: 170, weight: 70, age: 20, professions: "programmer")
   func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

     func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    func testConstructorLegal (){
              let person1 =  Person(name: "kaapo", height: 170, weight: 70, age: 20, professions: ["programmer"])
        
        
        
        XCTAssert(person1?.name == "kaapo")
           }
    
    func testIllegal (){
          let person2 =  Person(name: "kaapo", height: 180, weight: 70, age: 20, professions: ["programmer"])
        person2?.height = 500
        XCTAssert(person2!.height == 180,"height set too high")
        person2?.age = 19
        XCTAssert(person2!.age == 20, "age cant decrease")
        
       }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
       
    }

}
